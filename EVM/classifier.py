import numpy as np
from scipy.spatial.distance import cosine
from sklearn.base import BaseEstimator, ClassifierMixin
from sklearn.utils.validation import check_is_fitted, check_array, check_X_y

from . import MultipleEVM


class EvmClassifier(BaseEstimator, ClassifierMixin):
    _evm: MultipleEVM
    classes_: np.ndarray

    def __init__(
            self,
            tailsize: int = None,
            cover_threshold=None,
            cluster_centers=None,
            number_of_negatives=None,
            distance_multiplier=0.5,
            distance_function=cosine,
            include_cover_probability=False
    ):
        self.tailsize = tailsize
        self.cover_threshold = cover_threshold
        self.cluster_centers = cluster_centers
        self.number_of_negatives = number_of_negatives
        self.distance_multiplier = distance_multiplier
        self.distance_function = distance_function
        self.include_cover_probability = include_cover_probability

    def fit(self, X, y, parallel: int = None):
        X, y = check_X_y(X, y)

        self.classes_ = np.unique(y)

        self._evm = MultipleEVM(
            self.tailsize,
            cover_threshold=self.cover_threshold,
            cluster_centers=self.cluster_centers,
            number_of_negatives=self.number_of_negatives,
            distance_multiplier=self.distance_multiplier,
            distance_function=self.distance_function,
            include_cover_probability=self.include_cover_probability
        )

        self._evm.train([
            X[y == t, :] for t in self.classes_
        ], parallel)

        return self

    def predict_proba(self, X, parallel: int = None, norm: bool = True):
        check_is_fitted(self, ['classes_', '_evm'])

        # shape=(n_samples, n_classes, n_vectors)
        prob = self._evm.probabilities(check_array(X), parallel=parallel)

        # shape=(n_samples, n_classes)
        prob = np.asarray([list(map(max, sample_prob)) for sample_prob in prob])

        return norm_prob(prob, axis=1) if norm else prob

    def predict(self, X, parallel=None):
        return self.classes_[np.argmax(self.predict_proba(X, parallel=parallel), axis=1)]


def norm_prob(a, axis=0):
    div_shape = a.shape[:axis] + (1,) + a.shape[(axis + 1):]
    return a / np.sum(a, axis=axis).reshape(div_shape)
